library ( shiny )
library(ggplot2)

# Define server logic required to draw
shinyServer ( function (input , output ) {
  
  output$distplot <- renderPlot({
    # Generate gaussian distributor 
    x <- rnorm(input$numId)
    plot(x, xlab = 'x', ylab = 'y', main = 'distribution gaussienne', col = 'dark red')
    
  }
  )
  
  output$distplot2 <- renderPlot({
    # Generate gaussian distributor 
    x <- rnorm(input$numId)
    y <- sin(2*pi*x)
    df <- data.frame(x, y)
    
    # Draw a plot
    p <-ggplot(df, aes(x = x, y = y)) + geom_point(color=input$radioId) + 
      ggtitle(input$textId) + 
      theme(plot.title = element_text(hjust = .5, size=14, face="bold.italic"))
    
    q <- p + geom_line(color=input$radioId, size = input$epaisseurId)
    
    if (input$boundId == TRUE)
      q
    else
      p
  })
  
  # On va utiliser downloadHandler() qui prend deux arguments : filename et content
  output$plot <- downloadHandler(
    
    filename = function() {
      paste("df", input$var, sep = ".")
    },
    # content est la fonction avec l'argument file, écrit le plot
    content = function(file) {
      if (input$var == "jpg")
        jpg(file)
      else if (input$var == "png")
        png(file)
      else
        pdf(file)
      dev.off()
    }
  )
  

  
  
})