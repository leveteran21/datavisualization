#exercice nuage de points

library(ggplot2)
head(iris)
summary(iris)
p= ggplot(iris, aes(Sepal.Length,Sepal.Width,  colour = Species))
p+geom_point()
p+geom_point(aes(shape = Species))
p+geom_point(aes(shape = Species))+facet_grid(.~Species)
p=p+geom_point(aes(shape = Species))+facet_grid(.~Species)
p+ labs(title = "Linear modelling of Sepal.Length against Sepal.Width (IRIS) ", subtitle = "By Species")
p=p+ labs(title = "Linear modelling of Sepal.Length against Sepal.Width (IRIS) ", subtitle = "By Species")
p=p+scale_x_continuous(limits=c(4, 8))+scale_y_continuous(limits=c(2.0, 4.5))
p=p+theme_bw()
p=p+stat_smooth(method = "lm")




# extraire les coefficients du mod�le lin�aire
coef = lm(Sepal.Width ~ Sepal.Length:Species + Species - 1, data=iris)$coefficients
#data.frame avec les coordonn�es d'affichage
ann_text = data.frame(Sepal.Length = 7, Sepal.Width = 4,
                      intercept = coef[1:3],
                      a = coef[4:6], 
                      Species = c('setosa', 'versicolor', 'virginica'))
#les chaines de caract�res � afficher
eq1=paste0(round(ann_text$a[1],2), "x + ",round(ann_text$intercept[1],2))
eq2=paste0(round(ann_text$a[2],2), "x + ",round(ann_text$intercept[2],2))
eq3=paste0(round(ann_text$a[3],2), "x + ",round(ann_text$intercept[3],2))
#geom_text pour rajouter le calque
p+geom_text(data=ann_text, aes(label =c(eq1,eq2,eq3) ) )

