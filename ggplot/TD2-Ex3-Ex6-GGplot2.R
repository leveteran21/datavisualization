
#exercice nuage de points

library(ggplot2)
head(iris)
summary(iris)
p= ggplot(iris, aes(Sepal.Length,Sepal.Width,  colour = Species))
p+geom_point()
p+geom_point(aes(shape = Species))
p+geom_point(aes(shape = Species))+facet_grid(.~Species)
p=p+geom_point(aes(shape = Species))+facet_grid(.~Species)
p+ labs(title = "Linear modelling of Sepal.Length against Sepal.Width (IRIS) ", subtitle = "By Species")
p=p+ labs(title = "Linear modelling of Sepal.Length against Sepal.Width (IRIS) ", subtitle = "By Species")
p=p+scale_x_continuous(limits=c(4, 8))+scale_y_continuous(limits=c(2.0, 4.5))
p=p+theme_bw()
p=p+stat_smooth(method = "lm")



coef = lm(Sepal.Width ~ Sepal.Length:Species + Species - 1, data=iris)$coefficients

ann_text = data.frame(Sepal.Length = 7, Sepal.Width = 4,
                       intercept = coef[1:3],
                       a = coef[4:6], 
                       Species = c('setosa', 'versicolor', 'virginica'))


eq1=paste0(round(ann_text$a[1],2), "x + ",round(ann_text$intercept[1],2))
eq2=paste0(round(ann_text$a[2],2), "x + ",round(ann_text$intercept[2],2))
eq3=paste0(round(ann_text$a[3],2), "x + ",round(ann_text$intercept[3],2))
p+geom_text(data=ann_text, aes(label =c(eq1,eq2,eq3) ) )


#exercice histogramme
ggplot(iris, aes(Sepal.Length, fill=Species))+  
  geom_histogram(bins = 30) + 
  geom_bar(position = "dodge") +
  theme_bw()

library(plyr)
mu <- ddply(iris, "Species", summarise, grp.mean=mean(Sepal.Length))
head(mu)

# Changer la couleur des histogrammes par groupes
ggplot(iris, aes(x=Sepal.Length, color=Species)) +
  geom_histogram(bins = 30,fill="white")+
  theme_bw()
# Histogrammes ch�vauchants
ggplot(iris, aes(x=Sepal.Length, color=Species)) +
  geom_histogram(bins = 30,fill="white", alpha=0.5, position="identity")



# Histogrammes s�par�s
ggplot(iris, aes(x=Sepal.Length, color=Species)) +
  geom_histogram(fill="white", position="dodge")+
  theme(legend.position="top")
# Ajouter le trait de la moyenne
ggplot(iris, aes(x=Sepal.Length, color=Species)) +
  geom_histogram(bins = 30,fill="white", position="dodge")+
  geom_vline(data=mu, aes(xintercept=grp.mean, color=Species),
             linetype="dashed")+
  theme(legend.position="top")+
  geom_density(alpha=.6) 

ggplot(iris, aes(x=Sepal.Length, fill=Species, color=Species)) +
  theme(legend.position="top")+
  geom_density(alpha=.1) +geom_vline(data=mu, aes(xintercept=grp.mean, color=Species),
                                    linetype="dashed")


#exercice boite � moustache

 p=ggplot(iris, aes(Species, Petal.Length ))
 p+ geom_boxplot()  
  p + geom_boxplot(outlier.colour = "red", outlier.shape = 1)
  p + geom_boxplot(fill = "white", colour = "#3366FF") 
  p + geom_boxplot(aes(colour = Species))
  
  
  #s�ries temporelles
  class(economics)
  ggplot(economics, aes(date, unemploy)) + geom_line()
  ggplot(economics_long, aes(date, value01, colour = variable)) +
    geom_line()
 
  
  #cartes 

  crimes <- data.frame(state = tolower(rownames(USArrests)), USArrests)
  crimesm <- reshape2::melt(crimes, id = 1)
 library(maps)
    states_map <- map_data("state")
    ggplot(crimes, aes(map_id = state)) +
      geom_map(aes(fill = Murder), map = states_map) +
      expand_limits(x = states_map$long, y = states_map$lat)
    
    last_plot() + coord_map()
    ggplot(crimesm, aes(map_id = state)) +
      geom_map(aes(fill = value), map = states_map) +
      expand_limits(x = states_map$long, y = states_map$lat) +
      facet_wrap( ~ variable)

    